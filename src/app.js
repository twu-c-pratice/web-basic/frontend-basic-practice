import './styles/index.less';
import './js/main.js';
import 'jquery';

// async function fetchData(url) {
//   try {
//     const result = await fetch(url);
//     const data = await result.json();
//     // return data;
//     console.log(data);
//   } catch (e) {
//     console.error(e);
//   }
// }

function fetchData(url) {
  return fetch(url).then(response => response.json());
}

const url = 'http://localhost:3000/person';

fetchData(url)
  .then(result => {
    $('#header_h2').html(
      `MY NAME IS ${result.name} ${result.age}YO AND THIS IS MY RESUME/CV`
    );
    $('#about_me_detail').html(result.description);

    for (let education of result.educations) {
      const education_list = `
        <li class="education_list">
          <p class="year">${education.year}</p>
          <section class="experience">
            <h1 class="experience_title">${education.title}</h1>
            <p class="experience_detail">${education.description}</p>
          </section>
        </li>`;

      $('#education_ul').append(education_list);
    }
  })
  .catch(error => error);
